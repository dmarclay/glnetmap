import argparse
import threading, time

from packets import InterfacePacketSource, FilePacketSource, PacketSource

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--from-file', '-r', metavar='path', 
                    help='read from pcap file')
parser.add_argument('--interface', '-i', metavar='iface',
                    default="en0", help='sniff')
parser.add_argument('filters', metavar='rule', nargs="*",
                    help='tcpdump rules')

args = parser.parse_args()

if args.from_file:
    source = FilePacketSource(args.from_file, args)
else:
    source = InterfacePacketSource(args.interface, args)

from scene import App

class NetworkThread(threading.Thread):
    def __init__(self, app, packet_source):
        super(NetworkThread, self).__init__()
        self.app = app
        self.packet_source = packet_source

    def run(self):
        while self.app._is_running():
            try:
                p = self.packet_source.next()
            except PacketSource.Error:
                time.sleep(0.01)
                continue
            self.app.network.add(p)

app = App()
NetworkThread(app, source).start()
app.mainLoop()

