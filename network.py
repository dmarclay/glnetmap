import time, socket, threading

def _resolve(ip, _cache={}):
    if not _cache.has_key(ip):
        class X(threading.Thread):
            def run(self):
                try:
                    _cache[ip] = socket.gethostbyaddr(ip)[0]
                except:
                    del _cache[ip]
        _cache[ip] = ip ## chaep lock
        X().start()
    return _cache.get(ip, ip)


class Element(object):
    def __init__(self):
        self.created = time.time()
        self.activate()

    def activate(self):
        self.modified = time.time()
        return self.touch()

    def touch(self):
        self.touched = time.time()
        return self

    def get_age(self):
        return time.time() - self.created

    def get_activation_age(self):
        return time.time() - self.modified

    def get_contact_age(self):
        return time.time() - self.touched


class Node(Element):
    def __init__(self, ip):
        super(Node, self).__init__()
        self.ip = ip

    def __unicode__(self):
        return _resolve(self.ip)

class Route(Element):
    def __init__(self, src, dst):
        super(Route, self).__init__()
        self.src = src
        self.dst = dst
        self.packets = []

    def add(self, packet):
        self.packets.append(packet)

    def get_progress(self, packet):
        _fake_delay = 0.5
        return min(1.0, packet.get_age()/_fake_delay)

    def is_active(self, packet):
        return self.get_progress(packet)<1

class Packet(Element):
    def __init__(self, meta=None):
        super(Packet, self).__init__()
        self.meta = meta or {}

    def get_length(self):
        return self.meta.get("length", 0)

    def is_port(self, port):
        return port in (
            self.meta.get('sport', 0),
            self.meta.get('dport', 0))

    def is_http(self):
        return self.is_port(80)

class Network:
    def __init__(self):
        self.nodes = {}
        self.routes = {}

    def get_active_nodes(self):
        return (node for node in self.nodes.values()
                if node.get_contact_age()<10)
    
    def get_active_routes(self):
        return (route for (src, dst), route in self.routes.items()
                if route.get_activation_age()<10)

    def get_active_packets(self):
        return sum([r.packets for r in self.get_active_routes()], [])
    
    def _get_node(self, ip):
        if not self.nodes.has_key(ip):
            node = Node(ip)
            self.nodes[ip] = node
        else:
            node = self.nodes[ip]
        return node

    def _get_route(self, packet_info):
        src = self._get_node(packet_info["src"]).activate()
        dst = self._get_node(packet_info["dst"]).touch()
        _route_key = (src, dst)
        if not self.routes.has_key(_route_key):
            route = Route(src, dst)
            self.routes[_route_key] = route
        else:
            route = self.routes[_route_key].activate()
        return route

    def add(self, packet_info):
        route = self._get_route(packet_info)
        route.add(Packet(packet_info))

    def _collect_garbage(self):
        _max_age = 60
        dead_nodes = (k for k, n in self.nodes.items()
                      if n.get_activation_age()>_max_age)
        for k in dead_nodes:
            del self.nodes[k]

        dead_routes = (k for k, r in self.routes.items()
                      if r.get_activation_age()>_max_age)
        cnt = 0
        dead_cnt = 0
        for k, r in self.routes.items():
            if k in dead_routes: del self.routes[k]
            else:
                cnt += len(r.packets)
                dead_packets = (p for p in r.packets
                                if not r.is_active(p))
                for p in dead_packets:
                    r.packets.remove(p)
                    dead_cnt += 1
        ## print cnt, dead_cnt
