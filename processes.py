import psutil

class OpenConnectionsManager(object):
    def __init__(self):
        self.connections = {}
        self.snapshot()

    def _hash(self, d):
        return tuple([(k, d.get(k, None))
                for k in ('src', 'dst', 'sport', 'dport')])


    def set_process(self, data, p):
        self.connections[self._hash(data)] = p

    def get_process(self, data):
        return self.connections.get(self._hash(data), None)

    def snapshot(self):
        self.connections = {}
        pids = list(psutil.get_pid_list())
        for i, pid in enumerate(pids):
            try:
                p = psutil.Process(pid)
                connections = p.get_connections()
            except:
                continue
            for c in connections:
                if not c.local_address or not c.remote_address: continue
                _k = {
                    'src':c.local_address[0], 'sport':c.local_address[1],
                    'dst':c.remote_address[0], 'dport':c.remote_address[1]
                }
                self.set_process(_k, p)

    def get_emitter_process(self, packet):
        process = self.get_process(packet.meta)
        try:
            return {'pid':process.pid, 'name':process.name}
        except:
            return ## process died

        return self.connections.get(self._hash(packet.meta), None)
        ## def _is_part(p, c):
        ##     if not c.local_address or not c.remote_address: return
        ##     _c  = 
        ##     for k, v in _c.items():
        ##         if not p.meta.get(k, None) == v: return False
        ##     return True
        
        ## for process, c in self.connections:
        ##     if _is_part(packet, c):
        ##         return {'pid':process.pid, 'name':process.name}

open_connections_manager = OpenConnectionsManager()
