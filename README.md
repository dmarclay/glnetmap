# Overview

Cross platform network packet visualisation using a marton projection of IP space (like xkcb internet Map)

![Screenshot][1]

## Linux quickstart 

    $ sudo apt-get install libpcap-dev
    $ cd glnetmap
    $ virtualenv env
    $ env/bin/pip install -r requirements.txt
    $ sudo env/bin/python proto2.py -i eth0





[1]:https://bitbucket.org/dmarclay/glnetmap/raw/ca7368f1dfb1/doc/screenshot.png
