import pcapy
import impacket.ImpactDecoder

import time

class SimulatedPcapyReader:
    """add delays to to simulate pcap file packets timing"""
    def __init__(self, reader, time_scale=1):
        self._reader = reader
        self.time_scale = time_scale

    def next(self):
        h,d = self._reader.next()
	t,tt = h.getts()
        t = (t*1000000 + tt)/100000.0
        if hasattr(self, "_last_t"):
	    dt = t - self._last_t
	    time.sleep(dt*self.time_scale)
        self._last_t = t
        return h, d

    def setfilter(self, filter):
	self._reader.setfilter(filter)


class PacketSource(object):
    class Error(Exception): pass
    class UnknownPacket(Error): pass
    class NoPacket(Error): pass
        
    def __init__(self, reader, args):
        self.reader = reader
        self.args = args
	for filter in self.args.filters:
		self.reader.setfilter(filter)

    def next(self):
        try:
            header, data = self.reader.next()
        except pcapy.PcapError:
            raise PacketSource.NoPacket

        return self.decode(data)

    def decode(self, data):
        decoder = impacket.ImpactDecoder.EthDecoder()
        packet = decoder.decode(data)
        if packet.get_ether_type() == 2048:
            ip = packet.child()
            src, dst = ip.get_ip_src(), ip.get_ip_dst()
            length = ip.get_ip_len()
            data = {'src':src,
                    'dst':dst,
                    'length':length}
            try:
                data.update({
                    'sport':ip.child().get_th_sport(),
                    'dport':ip.child().get_th_dport(),
                              })
            except:
                pass
            return data
        else:
            if not packet.get_ether_type()==2054: # ARP
                print "UNKNOWN TYPE",  
                print packet
            raise PacketSource.UnknownPacket

class FilePacketSource(PacketSource):
    def __init__(self, path, args):
        r = SimulatedPcapyReader(pcapy.open_offline(path))
        super(FilePacketSource, self).__init__(r, args)

class InterfacePacketSource(PacketSource):
    def __init__(self, iface, args):
        r = pcapy.open_live(iface, 1600, 1, 100)
        super(InterfacePacketSource, self).__init__(r, args)
